<?php 
require 'Login.php';

class dbLoginCest 
{
     /**
     * @var \Codeception\Test\Unit
     */

    private $Database;

    public function setUp()
    {
        $this->Database = new Login();
}

    public function LoginWithDatabase(\Codeception\Test\Unit $test, UnitTester $I)
    {

        //get username & password
        $username = $this->Database->add(true);
        $password = $this->Database->add(false);
        
        //Encrypt...
        $ciphering = "AES-128-CTR"; 

        $iv_length = openssl_cipher_iv_length($ciphering); 
        $options = 0; 

        $encryption_iv = '1234567891011121'; 
        $encryption_key = "passwordchange"; 
        
        $encryption = openssl_encrypt($password, $ciphering, $encryption_key, $options, $encryption_iv); 

        // Grab password from db
        //$resultss = $I->grabFromDatabase('users', 'password', array('username' => $username));

        $I->seeInDatabase('users', ['username' => $username, 'password' => $encryption]);
    }
}  
 
