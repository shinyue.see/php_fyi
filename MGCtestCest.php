<?php 

class MGCtestCest
{
    public function _before(AcceptanceTester $I)
    {


        $I->amOnPage('/');
        $I->fillField('serial', 'w4232454e353');
        $I->click("View My Balance");
        $I->see('Invalid serial or pin code. If this problem persist, please contact us at ');
        $I->click("Buy MGC here");
        $I->amOnPage('/');
        $I->click("OffGamers");
        $I->amOnUrl('https://www.offgamers.com/game-card/universal-game-cards/multi-game-card-global');
        $I->click(['id' => 'c_1690070']);
        
        
    }

    // tests
    public function tryToTest(AcceptanceTester $I)
    {
    }
}
