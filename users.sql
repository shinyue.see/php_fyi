CREATE TABLE users (
id INT(4) AUTO_INCREMENT PRIMARY KEY,
username VARCHAR(255) NOT NULL,
password VARCHAR(255) NOT NULL
);

INSERT INTO users (id, username, password)VALUES (1001, 'admin', 'txt7wTo=');
INSERT INTO users (id, username, password)VALUES (1002, 'testing', 'ohpl3D1GdQ==');
INSERT INTO users (id, username, password)VALUES (1003, 'root', ' pBB53A==');
INSERT INTO users (id, username, password)VALUES (1004, 'number', ' uAp7yjFa');
